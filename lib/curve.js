'use strict';

const ExponentialRegression = require('ml-regression-exponential');
const dir = require('./dir');
const curves = dir.requireFromPrivateDir('curves.json');

const ln = (v) => ((console.log(v)), v);

const back = (v) => v[Math.max(v.length - 1, 0)];
const front = (v) => v[0];

const LinearRegression = (x, y) => ({
  predict: (n) => {
    const slope = (back(y) - front(y))/((back(x) - front(x)) || 1)
    const yInt = front(y) - slope*front(x);
    return slope*n + yInt;
  }
});

const makeCurve = (type) => {
  const regression = curves[type].type.match('linear') ? LinearRegression(curves[type].points.map((v) => v[0]), curves[type].points.map((v) => v[1])) : new ExponentialRegression((curves[type].points.map((v) => v[0])), (curves[type].points.map((v) => Math.max(v[1], 0.0001))));
  return (x) => regression.predict(x) + (curves[type].offset || 0);
};

Object.assign(module.exports, {
  makeCurve
});
