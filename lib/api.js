'use strict';

const {
  api
} = require('../config');
const url = require('url');
const {
  property
} = require('lodash');
const apiParsed = url.parse(api);
const fetch = require('./fetch');

Object.assign(module.exports, [
  'cancel',
  'order',
  'return24Volume',
  'returnBalances',
  'returnCompleteBalances',
  'returnContractAddress',
  'returnCurrencies',
  'returnDepositsWithdrawals',
  'returnNextNonce',
  'returnOpenOrders',
  'returnOrderBook',
  'returnOrderTrades',
  'returnTicker',
  'returnTradeHistory',
  'returnTradeHistoryMeta',
  'trade',
  'withdraw'
].reduce((r, v) => {
  r[v] = (payload = {}) => fetch({
    method: 'POST',
    url: url.format(Object.assign({}, apiParsed, {
      pathname: '/' + v
    })),
    json: payload
  }).then(property('body'))
    .then((body) => {
      if (body.error) throw Error(body.error);
      return body;
    });
  return r;
}, {}));
