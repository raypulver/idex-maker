'use strict';

const { createSelector } = require('reselect');
const { property } = require('lodash');

module.exports = createSelector([ property('tokens') ], (tokens) => Object.keys(tokens).reduce((r, v) => {
  const token = Object.assign({
    symbol: v
  }, tokens[v]);
  r[token.symbol] = token;
  r[token.address] = token;
  return r;
}, {}));
