'use strict';

const {
  toBuffer,
  addHexPrefix,
  bufferToHex
} = require('ethereumjs-util');
const {
  fromV3,
  fromPrivateKey
} = require('ethereumjs-wallet');
const readlineSync = require('readline-sync').question;
const {
  key,
  password,
  rpc
} = require('../config');
const Web3 = require('web3');
const store = require('./store');
const dir = require('./dir');
const {
  bindKey
} = require('lodash');
const { promisify } = require('bluebird');

const web3 = new Web3(new Web3.providers.HttpProvider(rpc));

const sendTransaction = promisify(bindKey(web3.eth, 'sendSignedTransaction'));
const getTransactionCount = promisify(bindKey(web3.eth, 'getTransactionCount'));

const loadWallet = () => dir.readFromPrivateDir(key || 'key.json').then(JSON.parse).then((key) => {
  if (typeof key === 'string') return fromPrivateKey(toBuffer(addHexPrefix(key)));
  return dir.readFromPrivateDir(password || 'password.txt')
    .then((password) => fromV3(key, password.trim()))
    .catch((err) => fromV3(key, readlineSync('Enter wallet password for ' + addHexPrefix(key.address) + ': ')));
});

Object.assign(module.exports, {
  loadWallet
});
