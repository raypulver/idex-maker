'use strict';

const bot = require('./bot');
const { mapSeries } = require('bluebird');

module.exports = ({
  subscribeToEvent
}) => subscribeToEvent('SET_LIQUI_TICKER', (action) => {
  mapSeries(Object.keys(action.payload), (v) => bot.resetPosition(v)).catch((err) => console.log(err.stack));
});
