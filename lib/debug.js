'use strict';

const {
  env
} = require('../config');

let isDebugging = env === 'development';
const startDebugInfo = () => (isDebugging = true);
const stopDebugInfo = () => (isDebugging = false);
const debugInspect = (v) => ((isDebugging ? console.log(require('util').inspect(v, { colors: true, depth: 15 })) : noop()), v);
const debug = (v) => ((isDebugging ? console.log(v) : noop()), v);

Object.assign(module.exports, {
  startDebugInfo,
  stopDebugInfo,
  debugInspect,
  debug
});
