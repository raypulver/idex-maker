'use strict';

const fetch = require('./fetch');
const { property } = require('lodash');

const getInfo = () => fetch({
  method: 'GET',
  url: 'https://api.liqui.io/api/3/info'
}).then(property('body')).then(JSON.parse);

const getTicker = (market) => {
  const marketRewrite = market.split('_').reverse().map((v) => v.toLowerCase()).join('_');
  return fetch({
    method: 'GET',
    url: 'https://api.liqui.io/api/3/ticker/' + marketRewrite
  }).then(property('body')).then(JSON.parse).then(property(marketRewrite));
};

Object.assign(module.exports, {
  getInfo,
  getTicker
});
