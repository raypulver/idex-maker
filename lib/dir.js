'use strict';

const { name } = require('../package');
const { join } = require('path');
const {
  mkdirp,
  readFile
} = require('fs-extra');
const {
  markets
} = require('../config');

const getPrivateDir = () => join(process.env.HOME, '.' + name);
const mkPrivateDir = () => mkdirp(getPrivateDir());
const readFromPrivateDir = (v) => mkPrivateDir().then(() => readFile(join(getPrivateDir(), v), 'utf8'));
const requireFromPrivateDir = (v) => require(join(getPrivateDir(), v));

const readTargetedMarkets = () => readFromPrivateDir(markets || 'markets.json').then(JSON.parse);

Object.assign(module.exports, {
  mkPrivateDir,
  readFromPrivateDir,
  readTargetedMarkets,
  requireFromPrivateDir
});
