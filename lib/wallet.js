'use strict';

const eth = require('./eth');
const store = require('./store');

const loadAndDispatchWallet = () => eth.loadWallet().then((wallet) => store.dispatch({
  type: 'SET_WALLET',
  payload: wallet
}));

const getAddress = () => store.getState().wallet.getAddressString();

Object.assign(module.exports, {
  loadAndDispatchWallet,
  getAddress
});
