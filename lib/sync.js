'use strict';

const api = require('./api');
const wallet = require('./wallet');
const dir = require('./dir');
const store = require('./store');
const {
  markets
} = require('../config');

const loadTargetedMarkets = () => dir.readTargetedMarkets().then((markets) => store.dispatch({
  type: 'SET_TARGETED_MARKETS',
  payload: markets
}));

const getAndDispatchNextNonce = () => api.returnNextNonce({
  address: wallet.getAddress()
}).then((nonce) => store.dispatch({
  type: 'SET_NONCE',
  payload: nonce
}));

const getAndDispatchContractAddress = () => api.returnContractAddress().then((contractAddress) => store.dispatch({
  type: 'SET_CONTRACT_ADDRESS',
  payload: contractAddress
}));

const getAndDispatchOrderBook = () => api.returnOrderBook().then((orders) => store.dispatch({
  type: 'SET_ORDERS',
  payload: orders
}));

const ln = (v) => ((console.log(v)), v);

const getAndDispatchSingleOrderBook = (market) => api.returnOrderBook({
  market
}).then((books) => store.dispatch({
  type: 'SET_SINGLE_BOOK',
  payload: {
    market,
    books
  }
})).catch(ln);

const getAndDispatchBalances = () => api.returnCompleteBalances({
  address: wallet.getAddress()
}).then((balances) => store.dispatch({
  type: 'SET_BALANCES',
  payload: balances
}));

const getAndDispatchTokens = () => api.returnCurrencies().then((tokens) => store.dispatch({
  type: 'SET_TOKENS',
  payload: tokens
}));

const getAndDispatchIDEXTicker = () => api.returnTicker().then((ticker) => store.dispatch({
  type: 'SET_IDEX_TICKER',
  payload: ticker
}));

const doSync = () => Promise.all([
  getAndDispatchNextNonce(),
  getAndDispatchContractAddress(),
  getAndDispatchOrderBook(),
  getAndDispatchBalances(),
  getAndDispatchTokens(),
  getAndDispatchIDEXTicker(),
  loadTargetedMarkets()
]);

Object.assign(module.exports, {
  doSync,
  getAndDispatchNextNonce,
  getAndDispatchContractAddress,
  getAndDispatchOrderBook,
  getAndDispatchBalances,
  getAndDispatchTokens,
  getAndDispatchIDEXTicker,
  getAndDispatchSingleOrderBook
});
