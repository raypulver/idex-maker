"use strict";

const {
  createStore,
  combineReducers,
  applyMiddleware
} = require('redux');
const {
  range
} = require('lodash');
const {
  readdirSync
} = require('fs');
const reduxHook = require('redux-hook');
const {
  parse,
  join
} = require('path');
const createSagaMiddleware = require('redux-saga').default;

const {
  middleware: reduxHookMiddleware,
  decorator
} = reduxHook({
  ignoreSubscribersProp: 'mutex'
}); 

const sagaMiddleware = createSagaMiddleware();

const store = decorator(createStore(combineReducers({
  spread: (state = range(10).map((v) => ({ width: 0.1, offset: 0.05 + v*0.025 })), action) => { 
    switch (action.type) {
      case 'SET_SPREAD':
        return action.payload;
    }
    return state;
  },
  targetedMarkets: (state = [], action) => {
    switch (action.type) {
      case 'SET_TARGETED_MARKETS':
        return action.payload;
    }
    return state;
  },
  contractAddress: (state = '', action) => {
    switch (action.type) {
      case 'SET_CONTRACT_ADDRESS':
        return action.payload;
    }
    return state;
  },
  nonce: (state = 0, action) => {
    switch (action.type) {
      case 'SET_NONCE':
        return action.payload;
      case 'INCREMENT_NONCE':
        return action.payload++;
    }
    return state;
  },
  orders: (state = {}, action) => {
    switch (action.type) {
      case 'SET_ORDERS':
        return action.payload;
      case 'SET_SINGLE_BOOK':
        return Object.assign({}, state, {
          [ action.payload.market ]: action.payload.books
        });
    }
    return state;
  },
  tokens: (state = [], action) => {
    switch (action.type) {
      case 'SET_TOKENS':
        return action.payload;
    }
    return state;
  },
  ethereumNonce: (state = 0, action) => {
    switch (action.type) {
      case 'SET_ETHEREUM_NONCE':
        return action.payload;
    }
    return state;
  },
  ethereumBalances: (state = 0, action) => {
    switch (action.type) {
      case 'SET_ETHEREUM_BALANCES':
        return action.payload;
    }
    return state;
  },
  currentBlock: (state = 0, action) => {
    switch (action.type) {
      case 'SET_CURRENT_BLOCK':
        return action.payload;
    }
    return state;
  },
  pairs: (state = {}, action) => {
    switch (action.type) {
      case 'SET_PAIRS':
        return action.payload;
    }
    return state;
  },
  wallet: (state = '', action) => {
    switch (action.type) {
      case 'SET_WALLET':
        return action.payload;
    }
    return state;
  },
  balances: (state = {}, action) => {
    switch (action.type) {
      case 'SET_BALANCES':
        return action.payload;
    }
    return state;
  },
  idexTicker: (state = {}, action) => {
    switch (action.type) {
      case 'SET_IDEX_TICKER':
        return action.payload;
    }
    return state;
  },
  liquiTicker: (state = {}, action) => {
    switch (action.type) {
      case 'SET_LIQUI_TICKER':
        return action.payload;
    }
    return state;
  }
}), applyMiddleware(
  reduxHookMiddleware,
  sagaMiddleware
)));

Object.assign(module.exports, store);
