'use strict';

const { soliditySha3 } = require('web3-utils');
const { mapValues } = require('lodash');
const {
  ecsign,
  toBuffer,
  bufferToHex,
  hashPersonalMessage
} = require('ethereumjs-util');
const store = require('./store');
const debug = require('./debug');
const wallet = require('./wallet');
const api = require('./api');
const BN = require('bignumber.js');
const { property } = require('lodash');
const selectTokens = require('./selectors/tokens');

const ethAddress = '0x' + Array(41).join(0);

const order = ({
  market,
  amount,
  type,
  price
}) => Promise.all([
  api.returnContractAddress().then(property('address')),
  api.returnNextNonce({
    address: wallet.getAddress()
  }).then(property('nonce'))
]).then(([ contractAddress, nonce ]) => {
  const [ base, trade ] = market.split('_');
  const tokens = selectTokens(store.getState());
  const tokenBuy = type === 'buy' ? tokens[trade].address : ethAddress;
  const tokenSell = type === 'sell' ? tokens[trade].address : ethAddress;
  const total = new BN(amount).mul(price);
  const amountBuy = new BN(type === 'buy' ? amount : total).mul(new BN(10).pow(tokens[tokenBuy].decimals)).floor().toPrecision();
  const amountSell = new BN(type === 'buy' ? total : amount).mul(new BN(10).pow(tokens[tokenSell].decimals)).floor().toPrecision();
  const expires = 100000;
  const address = wallet.getAddress();
  const raw = soliditySha3(...[{
    t: 'address',
    v: contractAddress
  }, {
    t: 'address',
    v: tokenBuy
  }, {
    t: 'uint256',
    v: amountBuy
  }, {
    t: 'address',
    v: tokenSell
  }, {
    t: 'uint256',
    v: amountSell
  }, {
    t: 'uint256',
    v: String(expires)
  }, {
    t: 'uint256',
    v: String(nonce)
  }, {
    t: 'address',
    v: address
  }]);
  const salted = hashPersonalMessage(toBuffer(raw));
  const {
    v,
    r,
    s
  } = mapValues(ecsign(salted, store.getState().wallet.getPrivateKey()), (value, key) => key === 'v' ? value : bufferToHex(value));
  return api.order({
    tokenBuy,
    amountBuy,
    tokenSell,
    amountSell,
    address,
    nonce,
    expires,
    v,
    r,
    s
  });
});

const cancel = ({
  orderHash
}) => api.returnNextNonce({
  address: wallet.getAddress()
}).then(property('nonce')).then((nonce) => {
  const raw = soliditySha3({
    t: 'bytes32',
    v: orderHash
  }, {
    t: 'uint256',
    v: String(nonce)
  });
  const salted = hashPersonalMessage(toBuffer(raw));
  const {
    v,
    r,
    s
  } = mapValues(ecsign(salted, store.getState().wallet.getPrivateKey()), (value, key) => key === 'v' ? value : bufferToHex(value));
  return api.cancel({
    orderHash,
    address: wallet.getAddress(),
    nonce,
    v,
    r,
    s
  });
});

Object.assign(module.exports, {
  order,
  cancel
});
