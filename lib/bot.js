'use strict';

const eth = require('./eth');
const wallet = require('./wallet');
const sync = require('./sync');
const ticker = require('./ticker');
const store = require('./store');
const signed = require('./signed');
const BN = require('bignumber.js');
const requestDebug = require('request-debug');
const { mapSeries } = require('bluebird');
const {
  env,
  precisionLossAdjust
} = require('../config');
const {
  range,
  noop
} = require('lodash');
const curve = require('./curve');
const debug = require('./debug');
//if (env === 'development') requestDebug(require('request'));

BN.config({
  ERRORS: false,
  DECIMAL_PLACES: 100
});

let busy = false;

const isPerformingAction = () => busy;

const startBot = () => wallet.loadAndDispatchWallet().then(() => sync.doSync()).then(() => {
  ticker.startSyncingWithLiqui();
}).then(() => {
  require('./move-position')(store);
});

const clearPosition = (market) => {
  const {
    orders
  } = store.getState();
  const books = orders[market] || { bids: [], asks: [] };
  const myOrders = books.bids.concat(books.asks).filter((v) => v.params.user === wallet.getAddress());
  return mapSeries(myOrders, (v) => signed.cancel({
    orderHash: v.orderHash
  })).then(() => sync.getAndDispatchSingleOrderBook(market));
};

const calcSpread = (config) => (range(config.orders - 1).map((v) => ((v + 1)/(config.orders - 1)))).map((() => {
  let total = 0;
  const priceCurve = curve.makeCurve(config.offset);
  const widthCurve = curve.makeCurve(config.width);
  return (v, i, ary) => {
    const width = i === ary.length - 1 ? 1 : Math.min(1, widthCurve(v));
    const segment = (width - total)/((precisionLossAdjust + 1) || 1.05);
    total = width;
    const offset = priceCurve(v);
    return {
      width: segment,
      offset
    };
  };
})()).filter((v) => v.width !== 0);

const resetPosition = (market) => isPerformingAction() ? Promise.resolve() : clearPosition(market).then(() => sync.getAndDispatchBalances()).then(() => {
  const {
    targetedMarkets,
    tokens,
    balances,
    orders,
    liquiTicker
  } = store.getState();
  const ticker = liquiTicker[market];
  if (!ticker) return;
  const [ base, trade ] = market.split('_');
  const marketConfig = targetedMarkets.find((v) => v.market === market);
  const buySpread = calcSpread(marketConfig.buy);
  const sellSpread = calcSpread(marketConfig.sell);
  let baseAmount = new BN(balances[base].available).add(balances[base].onOrders).mul(targetedMarkets.find((v) => v.market === market).appropriation).toPrecision();
  let tradeAmount = new BN(balances[trade].available).add(balances[base].onOrders).toPrecision();
  busy = true;
  return mapSeries(buySpread, (v) => {
    if (new BN(v.width).lte(0)) return Promise.resolve();
    const priceCurve = curve.makeCurve(marketConfig.buy.offset);
    const price = new BN(ticker.last).mul(new BN(1).minus(v.offset)).toPrecision();
    const amount = new BN(baseAmount).mul(v.width).divToInt(price).minus(0.1).floor().toPrecision();
    return signed.order(debug.debugInspect({
      type: 'buy',
      market,
      price,
      amount
    })).catch(noop);
  }).then(() => mapSeries(sellSpread, (v) => {
    if (new BN(v.width).lte(0)) return Promise.resolve();
    const price = new BN(ticker.last).mul(new BN(1).add(v.offset)).toPrecision();
    const amount = new BN(tradeAmount).mul(v.width).floor().minus(0.1).floor().toPrecision();
    return signed.order(debug.debugInspect({
      type: 'sell',
      market,
      price,
      amount
    })).catch(noop);
  })).then(() => sync.getAndDispatchSingleOrderBook(market)).then(() => (busy = false));
});

Object.assign(module.exports, {
  startBot,
  clearPosition,
  resetPosition,
  isPerformingAction
});
