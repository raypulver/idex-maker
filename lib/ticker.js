'use strict';

const store = require('./store');
const liqui = require('./liqui');
const {
  tickerPollInterval
} = require('../config');
const {
  timeout
} = require('./promise');
const { mapSeries } = require('bluebird');
const bot = require('./bot');
const {
  zipObject,
  property,
  noop
} = require('lodash');

const fetchTickers = () => {
  const markets = store.getState().targetedMarkets.map(property('market'));
  return mapSeries(markets, (v) => liqui.getTicker(v)).then((tickers) => zipObject(markets, tickers));
};

const fetchAndDispatchTickers = () => bot.isPerformingAction() ? Promise.resolve(false) : fetchTickers().then((tickers) => store.dispatch({
  type: 'SET_LIQUI_TICKER',
  payload: tickers
}));

const startSyncingWithLiqui = () => fetchAndDispatchTickers().catch(noop).then(() => timeout(tickerPollInterval)).then(startSyncingWithLiqui);

Object.assign(module.exports, {
  fetchTickers,
  fetchAndDispatchTickers,
  startSyncingWithLiqui
});
