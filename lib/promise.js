'use strict';

Object.assign(module.exports, {
  timeout: (n) => new Promise((resolve, reject) => setTimeout(resolve, n))
});
