# idex-maker

Welcome to the scantily documented, highly successful idex-maker bot. If you found this page then you are most likely interested in how we do market making at IDEX, or perhaps simply want to run the bot for yourself without modifying it to your use case. In either case you should learn about what the bot actually does and its current limitations.

## Quick start

Install a reasonably recent Node.js LTS distribution

```shell
git clone https://bitbucket.org/raypulver/idex-maker
cd idex-maker
npm install
cp -rf sample-configs ~/.idex-maker
```

Replace `~/.idex-maker/key.json` with a MEW compatible wallet and store a plaintext password for the wallet in `~/.idex-maker/password.txt`

```shell
npm start
```

## Customization

You will probably want to adjust what markets you want to market make on, this will be in `~/.idex-maker/markets.json`, where you will notice that markets are in the form of `BASE_TRADE`, and you can set an `appropriation` field for each market representing the amount of ETH of your total supply you are market making with. For each market you can configure a `buy` and `sell` property which allow you to set what curve you want to use for your `width` (volume of coins being offered depending on how far into the order making the process is from 0-1) and your `offset` (price of the order you are posting).

Curves are defined in `~/.idex-maker/curves.json` and can either be linear (not tested) or exponential. Exponential curves are defined with an arbitrary amount of x, y coordinate pairs and some intercept for the y-axis defined in `offset`. The curve itself is an exponential regression and may not touch these points exactly depending on how dramatic you want your curve to be. The x coordinate in these points is essentially a number line from 0 to 1, and the regression will be sampled along this domain depending on how many orders across which you want to spread your position, i.e. if you want 20 orders on a market then the regression will be sampled at 0, 0.05, 0.1, 0.15, etc. The y-coordinate represents something different depending on if the curve is being applied to the `width` or `offset`, if it is applied to the `width` then it represents how much of the total supply should be on the books by the time it reaches 0.05/1 of the orders to be posted, 0.1/1 of the orders to be posted, etc, where a y-coordinate of 0 means none of the supply and 1 means the whole thing. When applied to the price offset, it determines how far away from the fair value of the asset (as determined by the price feed for that market) it should price the order. The default curves in idex-maker for the `width` might be appropriate for typical use cases but if you desire to minimize your risk and maximize your profit margin you will most likely want to define your own price curve, since the default one sets the initial spread at 0.01 (1% from fair value)

## Limitations

Currently the price feed comes exclusively from liqui.io, and there is no interface to provide your own price feed adapters. If you have a token which you want the price to stay around a constant area, you can simply overwrite the logic in `lib/ticker.js`. If you really like idex-maker and want to expand on the project, generalizing the logic in the ticker module to support custom price feeds would make a major difference to the usefulness of the application.

The orderbook is only reset every time a ticker update comes in, it does not react to traders filling orders made by the bot, nor does it rebalance funds across different exchanges. It will also completely clear your position and reset during a ticker update, which is not ideal. If you like idex-maker and want to contribute, a subsystem for avoiding these subtleties would bring great elegance to the project. If you like idex-maker but want the code for yourself to modify, that is fine too, but you may find support is more limited from the team.

The exponential regression supplied to the `width` calculation for the volume on each order is not adjusted for the actual cumulative amount calculated by sampling the curve at each discrete point, so you will most likely post all your orders and still have some funds left over. If you love idex-maker too much to let this be the case, we invite you to fork us and patch the program to multiply the actual volume posted per order by some factor calculated based on the actual total amount available in your trading balance, and the total amount calculated by sampling the `width` curve.

Best of luck to all idex-makers, may your limit orders be swift and true.

## Author
Raymond Pulver IV
