#!/usr/bin/env node

'use strict';

const bot = require('./lib/bot');
const repl = require('repl');
const {
  join,
  parse
} = require('path');
const { camelCase } = require('lodash');
const assign = require('object-assign');
const {
  readdir
} = require('fs-extra');

bot.startBot().then(() => readdir(join(__dirname, 'lib'))).then((libs) => {
  const r = repl.start('> ');
  assign(r.context, libs.filter((v) => parse(v).ext === '.js').reduce((r, v) => {
    r[camelCase(parse(v).name)] = require(join(__dirname, 'lib', v));
    return r;
  }, {}));
}).catch((err) => console.log(err.stack));
